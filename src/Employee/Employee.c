#include "Employee.h"

#include "../List/List.h"
#include <string.h>

Employee* Employee_New(int code, const char* name, const char* sector)
{
    Employee* employee = malloc(sizeof(Employee));

    employee->Code = code;
    
    employee->Name = malloc(strlen(name) * sizeof(char));
    strcpy(employee->Name, name);

    employee->Sector = malloc(strlen(sector) * sizeof(char));
    strcpy(employee->Sector, sector);

    return employee;
}

static void FreeEmployee(void* pointer)
{
    Employee* employee = pointer;

    free(employee->Name);
    free(employee->Sector);

    free(employee);
}

static bool EmployeeEqualityFunction(void* employee_pointer, void* code_pointer)
{
    Employee* employee = employee_pointer;
    int code = *(int*)code_pointer;

    return employee->Code == code;
}

List* Employee_NewEmployeeList()
{
    return List_New(sizeof(Employee*), FreeEmployee, true);
}

void Employee_AddToList(List* list, Employee* employee)
{
    int code;
    if(list->Length == 0)
    {
        code = 1;
    }
    else
    {
        Employee* last_employee = list->Tail->Data;
        code = last_employee->Code + 1;
    }

    employee->Code = code;
    
    List_Append(list, employee);
}

void Employee_RemoveFromList(List* list, int code)
{
    List_Remove(list, EmployeeEqualityFunction, (void*)&code);
}

Employee* Employee_FindInList(List* list, int code)
{
    return List_Find(list, EmployeeEqualityFunction, (void*)&code);
}