#include "UI.h"
#include <stdio.h>
#include <stdlib.h>
#include "../curses.h"
#include "../String/String.h"

static void ClearRigthOf(int y, int x)
{
    move(y,x);
    clrtoeol();
}

void InitializeColors()
{
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
}

void ClearInputBuffer()
{
    fflush(stdin);
}

void Clear()
{
    clear();
}

void Pause()
{
    system("pause");
}

void PrintTitle(const char* title)
{
    Clear();
    printw(" --- %s ---\n\n", title);
    refresh();
}

void PromptForString(const char* message, char* destination, int length)
{    
    printf("%s: ", message);
    
    ClearInputBuffer();
    fgets(destination, length, stdin);
}

void PromptForNonEmptyString(const char* message, char* destination, int length)
{    
    char* input = calloc(length, sizeof(char));

    printw("%s: ", message);
    
    int x = getcurx(stdscr);
    int y = getcury(stdscr);
        
    while(true)
    {
        ClearRigthOf(y,x);

        getstr(input);

        if(!String_IsEmptyOrWhitespace(input))
        {
            break;
        }

        ClearRigthOf(y,x);
        ShowError("ENTRADA INVÁLIDA! NÃO PODE SER VAZIA OU APENAS ESPAÇOS!");        
        refresh();

        getch();
    }

    strcpy(destination, input);
}

void PromptForInt(const char* message, int* destination)
{
    printw("%s: ", message);
    scanw("%d", &destination);
    scanf("%d", destination);
}

void PromptForPositiveInt(const char* message, int* destination)
{    
    int input;
    printw("%s: ", message);
    
    int x = getcurx(stdscr);
    int y = getcury(stdscr);
        
    while(true)
    {
        ClearRigthOf(y,x);
        scanw("%d", &input);

        if(input >= 0)
        {
            break;
        }

        ClearRigthOf(y,x);        
        ShowError("ENTRADA INVÁLIDA! NÃO PODE SER NÚMERO NEGATIVO!");        
        getch();
    }

    *destination = input;
}

void PromptForGreaterThanZeroInt(const char* message, int* destination)
{    
    int input;
    printw("%s: ", message);
    
    int x = getcurx(stdscr);
    int y = getcury(stdscr);
        
    while(true)
    {
        ClearRigthOf(y,x);
        scanw("%d", &input);

        if(input > 0)
        {
            break;
        }

        ClearRigthOf(y,x);        
        ShowError("ENTRADA INVÁLIDA! DEVE SER MAIOR QUE ZERO!");        
        getch();
    }

    *destination = input;
}

void PromptForNegativeInt(const char* message, int* destination)
{    
    int input;
    printw("%s: ", message);
    
    int x = getcurx(stdscr);
    int y = getcury(stdscr);
        
    while(true)
    {
        ClearRigthOf(y,x);
        scanw("%d", &input);

        if(input < 0)
        {
            break;
        }

        ClearRigthOf(y,x);        
        ShowError("ENTRADA INVÁLIDA! NÃO PODE SER NÚMERO POSITIVO!");        
        getch();
    }

    *destination = input;
}