#ifndef __EMPLOYEE_HEADERFILE
#define __EMPLOYEE_HEADERFILE

#include "../List/List.h"

typedef struct Employee
{
    int Code;
    char* Name;
    char* Sector;
} Employee;

Employee* Employee_New(int code, const char* name, const char* sector);

List* Employee_NewEmployeeList();
void Employee_AddToList(List* list, Employee* employee);
void Employee_RemoveFromList(List* list, int code);
Employee* Employee_FindInList(List* list, int code);

#endif