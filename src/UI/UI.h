#ifndef __UI_HEADERFILE
#define __UI_HEADERFILE

#include <stdio.h>

void ClearInputBuffer();
void Clear();
void Pause();

#define ERROR_COLOR_PAIR COLOR_PAIR(1)
#define WARN_COLOR_PAIR COLOR_PAIR(2)
#define SUCESS_COLOR_PAIR COLOR_PAIR(3)

#define ShowError(message) {\
    attron(ERROR_COLOR_PAIR);\
    printw("%s", message);\
    attroff(ERROR_COLOR_PAIR);\
    beep();\
    refresh();\
}

#define ShowWarning(message) {\
    attron(WARN_COLOR_PAIR);\
    printw("%s", message);\
    attroff(WARN_COLOR_PAIR);\
    refresh();\
}

#define ShowSucess(message) {\
    attron(SUCESS_COLOR_PAIR);\
    printw("%s", message);\
    attroff(SUCESS_COLOR_PAIR);\
    refresh();\
}

void InitializeColors();

void PrintTitle(const char* title);

void PromptForString(const char* message, char* destination, int length);
void PromptForNonEmptyString(const char* message, char* destination, int length);

void PromptForInt(const char* message, int* destination);
void PromptForPositiveInt(const char* message, int* destination);
void PromptForGreaterThanZeroInt(const char* message, int* destination);
void PromptForNegativeInt(const char* message, int* destination);

#endif