#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "List.h"

List* List_New(size_t element_size, FreeFunction free_function, bool is_list_of_pointers)
{
    List* list = malloc(sizeof(List));

    list->Length = 0;
    list->IsListOfPointers = is_list_of_pointers;
    list->ElementSize = element_size;
    list->FreeFunction = free_function;
    list->Head = NULL;
    list->Tail = NULL;

    return list;
}

void List_Destroy(List* list)
{
    Node* current;
    while(list->Head != NULL)
    {
        current = list->Head;
        list->Head = current->Next;

        if(list->FreeFunction)
        {
            list->FreeFunction(current->Data);
        }

        free(current->Data);
        free(current);
    }
}

static Node* AllocateNewNode(List* list)
{
    Node* node = malloc(sizeof(Node));

    node->Data = malloc(sizeof(list->ElementSize));
    node->Next = NULL;

    return node;
}

void List_Append(List* list, void* element)
{
    Node* node = AllocateNewNode(list);

    if(list->IsListOfPointers)
    {
        node->Data = element;
    }
    else
    {
        memcpy(node->Data, element, list->ElementSize);
    }

    if(list->Length == 0)
    {
        list->Head = node;        
        list->Tail = node;
    }
    else
    {
        list->Tail->Next = node;
        list->Tail = node;
    }    

    list->Length++;
}

void List_Prepend(List* list, void* element)
{
    Node* node = AllocateNewNode(list);

    if(list->IsListOfPointers)
    {
        node->Data = element;
    }
    else
    {
        memcpy(node->Data, element, list->ElementSize);
    }

    if(list->Length == 0)
    {
        list->Tail = node;
    }

    node->Next = list->Head;
    list->Head = node;

    list->Length++;    
}

void List_Remove(List* list, EqualityFunction compare, void* element)
{
    if(list->Length == 0)
    {
        return;
    }

    Node* previous = NULL;
    Node* current = list->Head;
    
    bool found = compare(current->Data, element);
    while(!found)
    {
        previous = current;
        current = current->Next;

        if(current == NULL)
        {
            return;
        }
        
        found = compare(current->Data, element);
    }

    if(previous != NULL)
    {
        previous->Next = current->Next;
    }    

    if(current == list->Head)
    {
        list->Head = current->Next;
    }
    
    if(current == list->Tail)
    {
        list->Tail = previous;
    }

    if(list->FreeFunction != NULL)
    {
        list->FreeFunction(current->Data);
    }   
     
    free(current);
}
void List_RemoveAtIndex(List* list, int index);

Node* List_AtIndex(List* list, int index)
{
    if(index >= list->Length)
    {
        return NULL;
    }

    Node* current = list->Head;

    for(int i = 0; i < index; i++)
    {
        if(current == NULL)
        {
            return NULL;
        }

        current = current->Next;
    }

    return current;
}

void* List_Find(List* list, EqualityFunction compare, void* value)
{
    Node* current = list->Head;

    for(int i = 0; i < list->Length; i++)
    {
        void* current_data = current->Data;

        if(compare(current_data, value))
        {
            return current_data;
        }

        current = current->Next;
    }

    return NULL;
}

List* List_FindMany(List* list, EqualityFunction compare, void* value)
{
    List* result = List_New(list->ElementSize, list->FreeFunction, list->IsListOfPointers);

    Node* current = list->Head;
    for(int i = 0; i < list->Length; i++)
    {
        if(compare(current->Data, value))
        {
            List_Append(result, current->Data);
        }

        current = current->Next;
    }

    if(result->Length > 0)
    {
        return result;
    }

    return NULL;
}

void List_ForEach(List* list, Iterator iterator)
{
    Node* node = list->Head;
    
    int resume = 1;

    while(node != NULL && resume)
    {
        resume = iterator(node->Data);
        node = node->Next;
    }
}

static void _Swap(void *a, void *b, size_t size)
{
    uint8_t previous_byte;
    uint8_t * a_byte = a;
    uint8_t * b_byte = b;

    
    for(size_t i = 0; i < size; i++)
    {
        previous_byte = a_byte[i];
        a_byte[i] = b_byte[i];
        b_byte[i] = previous_byte;
    }    
}

int CompareFloat(float a, float b)
{
    if(a > b)
    {
        return 1;
    }
    if(a < b)
    {
        return -1;
    }

    return 0;
}

int CompareInt(int a, int b)
{
    if(a > b)
    {
        return 1;
    }
    if(a < b)
    {
        return -1;
    }

    return 0;
}

void List_Sort(List* list, SortingOrder order, CompareFunction compare)
{
    for(int wall = list->Length - 1; wall > 0; wall--)
    {
        Node* current_node = list->Head;
        for(int current = 0; current < wall; current++)
        {
            Node* next_node = current_node->Next;

            int comparisson = compare(current_node->Data, next_node->Data);

            if((order == ASCENDING && comparisson > 0) || (order == DESCENDING && comparisson < 0))
            {
                _Swap(current_node->Data, next_node->Data, list->ElementSize);
            }

            current_node = current_node->Next;
        }
    }
}

void* List_Pop(List* list)
{
    Node *node = list->Head;
    
    list->Head = node->Next;
    list->Length--;

    void* data = malloc(list->ElementSize);
    memcpy(data, node->Data, list->ElementSize);

    free(node->Data);
    free(node);    

    return data;
}