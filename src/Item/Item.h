#ifndef __ITEM_HEADERFILE
#define __ITEM_HEADERFILE

#include "../List/List.h"

typedef struct Item
{
    int Code;
    char* Name;
    int Quantity;
} Item;

Item* Item_New(int code, const char* name, int quantity);

List* Item_NewItemList();
void Item_AddToList(List* list, Item* item);
void Item_RemoveFromList(List* list, int code);
Item*Item_FindInList(List* list, int code);

#endif