#ifndef __STRING_HEADERFILE
#define __STRING_HEADERFILE

#include <string.h>
#include <stdbool.h>

bool String_IsEmptyOrWhitespace(const char* string);

#endif