#ifndef __TRANSACTION_HEADERFILE
#define __TRANSACTION_HEADERFILE

#include "../Item/Item.h"
#include "../Employee/Employee.h"
#include "../List/List.h"
#include <time.h>

typedef enum TransactionType
{
    Add,
    Remove
} TransactionType;

typedef struct Transaction
{
    time_t* Date;
    Employee* Employee;
    Item* Item;
    TransactionType Type;
    int Quantity;
} Transaction;

Transaction* Transaction_New(Employee* employee, Item* item, TransactionType type, int quantity);

List* Transaction_NewTransactionList();
void Transaction_AddToList(List* list, Transaction* transaction);

List* Transaction_FindByItem(List* list, Item* item);
List* Transaction_FindByEmployee(List* list, Employee* employee);

#endif