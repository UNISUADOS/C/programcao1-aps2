#ifndef __LIST_HEADERFILE
#define __LIST_HEADERFILE

#include <stdlib.h>
#include <stdbool.h>

typedef void (*FreeFunction)(void*);
typedef bool (*Iterator)(void*);
typedef bool (*EqualityFunction)(void*, void*);

typedef struct _Node {
    void* Data;
    struct _Node* Next;
} Node;

typedef struct _List {
    int Length;
    int ElementSize;
    bool IsListOfPointers;

    struct _Node* Head;
    struct _Node* Tail;

    FreeFunction FreeFunction;
} List;

List* List_New(size_t element_size, FreeFunction free_function, bool is_list_of_pointers);
void List_Destroy(List* list);

void List_Append(List* list, void* element);
void List_Prepend(List* list, void* element);

void List_Remove(List* list, EqualityFunction compare, void* element);
void List_RemoveAtIndex(List* list, int index);

Node* List_AtIndex(List* list, int index);
void* List_Find(List* list, EqualityFunction compare, void* value);
List* List_FindMany(List* list, EqualityFunction compare, void* value);
void List_ForEach(List* list, Iterator iterator);

int CompareFloat(float a, float b);
int CompareInt(int a, int b);

typedef int (*CompareFunction)(void*, void*);
typedef enum SortingOrder { ASCENDING = 1, DESCENDING = 2} SortingOrder;
void List_Sort(List* list, SortingOrder order, CompareFunction compare);

void* List_Pop(List* list);

#endif