#ifndef __MENU_HEADERFILE
#define __MENU_HEADERFILE

#include <stdbool.h>
#include "../List/List.h"

#define EXIT 0

typedef void (*Action)();

typedef struct Option
{
    char* Title;    
    List* SubOptions;
    bool IsSubMenu;
    Action Action;
} Option;

typedef struct Menu
{
    char* Title;
    List* Options;
} Menu;

Menu* Menu_New(char* title);
void Menu_Show(Menu* menu);
void Menu_AddOption(Menu* menu, Option* option);

Option* Option_New(char* title, Action action);
Option* Option_NewSubMenu(char* title);
void Option_AddSubOption(Option* subMenu, Option* subOption);

#endif