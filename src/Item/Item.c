#include "Item.h"

#include <string.h>
#include <stdbool.h>
#include "../List/List.h"

Item* Item_New(int code, const char* name, int quantity)
{
    Item* item = malloc(sizeof(Item));
    item->Name = malloc(strlen(name) * sizeof(char));

    item->Code = code;
    strcpy(item->Name, name);
    item->Quantity = quantity;

    return item;
}

static void FreeItem(void* pointer)
{
    Item* item = pointer;

    free(item->Name);
    free(item);
}

static bool ItemEqualityFunction(void* item_pointer, void* code_pointer)
{
    Item* item = item_pointer;
    int id = (*(int*)code_pointer);

    return item->Code == id;
}

List* Item_NewItemList()
{
    return List_New(sizeof(Item*), FreeItem, true);
}

void Item_AddToList(List* list, Item* item)
{
    List_Append(list, item);
}

void Item_RemoveFromList(List* list, int code)
{
    List_Remove(list, ItemEqualityFunction, (void*)&code);
}

Item* Item_FindInList(List* list, int code)
{
    return List_Find(list, ItemEqualityFunction, (void*)&code);
}