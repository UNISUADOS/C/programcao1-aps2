#include "String.h"
#include <ctype.h>
bool String_IsEmptyOrWhitespace(const char* string)
{
    int length = strlen(string);
    for(int i = 0; i < length; i++)
    {
        char character = string[i];

        if(character != ' ')
        {
            return false;            
        }
    }

    return true;
}