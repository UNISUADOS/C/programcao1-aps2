#include "../curses.h"
#include "Menu.h"
#include "../UI/UI.h"
#include <string.h>
#include <stdio.h>
#include "../List/List.h"

Menu* Menu_New(char* title)
{
    Menu* menu = malloc(sizeof(Menu));
    
    menu->Options = List_New(sizeof(Option*), NULL, true);
    
    menu->Title = malloc(strlen(title) * sizeof(char));
    strcpy(menu->Title, title);

    return menu;
}

static void PrintOptions(List* options, bool isSubMenu)
{
    Node* current = options->Head;

    const char format[] = "[%d] %s\n";
    
    for(int i = 0; i < options->Length; i++)
    {
        Option* option = current->Data;
        printw(format, i+1, option->Title);

        current = current->Next;
    }

    printw("\n");

    if(isSubMenu)
    {
        printw(format, EXIT, "VOLTAR");
    }
    else
    {
        printw(format, EXIT, "SAIR");
    }
    
    refresh();
}

static int GetUserChoice(List* options, bool isSubMenu)
{
    PrintOptions(options, isSubMenu);

    putchar('\n');

    printw("ESCOLHA: ");

    int choice;
    scanw("%d", &choice);

    return choice;
}

static Option* GetOptionFromChoice(List* options, int choice)
{
    if(choice < 0 || choice > options->Length)
    {
        return NULL;
    }

    int option_index = choice - 1;

    return List_AtIndex(options, option_index)->Data;
}

static void ShowOptions(List* options, const char* title, bool isSubMenu)
{
    int choice = 1;
    
    while(choice != EXIT)
    {
        PrintTitle(title);
        int choice = GetUserChoice(options, isSubMenu);

        if(choice == EXIT)
        {
            break;
        }

        Option* selected_option = GetOptionFromChoice(options, choice);

        if(selected_option == NULL)
        {
            continue;
        }

        if(selected_option->IsSubMenu)
        {
            ShowOptions(selected_option->SubOptions, selected_option->Title, true);
            continue;
        }

        PrintTitle(selected_option->Title);
        selected_option->Action();
    }
}

void Menu_Show(Menu* menu)
{
    ShowOptions(menu->Options, menu->Title, false);
}

void Menu_AddOption(Menu* menu, Option* option)
{
    List_Append(menu->Options, option);
}

Option* Option_New(char* title, Action action)
{
    Option* option = malloc(sizeof(Option));

    option->Action = action;
    option->IsSubMenu = false;
    option->SubOptions = NULL;

    option->Title = malloc(strlen(title) * sizeof(char));
    strcpy(option->Title, title);

    return option;
}

Option* Option_NewSubMenu(char* title)
{
    Option* option = malloc(sizeof(Option));

    option->Action = NULL;
    option->IsSubMenu = true;
    option->SubOptions = List_New(sizeof(Option*), NULL, true);

    option->Title = malloc(strlen(title) * sizeof(char));
    strcpy(option->Title, title);

    return option;
}

void Option_AddSubOption(Option* subMenu, Option* subOption)
{
    List_Append(subMenu->SubOptions, subOption);
}