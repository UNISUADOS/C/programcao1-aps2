#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#include "curses.h"

#include "List/List.h"
#include "Item/Item.h"
#include "Menu/Menu.h"
#include "Employee/Employee.h"
#include "Transaction/Transaction.h"
#include "UI/UI.h"
#include "String/String.h"


List* employees;
List* items;
List* transactions;

//EMPLOYEE FUNCTIONS
bool Print_Employee(void* data)
{
    Employee* employee = data;

    printw("CÓDIGO: %d\n", employee->Code);
    printw("NOME  : %s\n", employee->Name);
    printw("SETOR : %s\n\n", employee->Sector);

    return true;
}

void Action_AddEmployee()
{
    char title[] = "CADASTRAR FUNCIONÁRIO";
    PrintTitle(title);

    char name[100];
    char sector[100];

    PromptForNonEmptyString("NOME ", name, 100);
    PromptForNonEmptyString("SETOR", sector, 100);

    Employee_AddToList(employees, Employee_New(0, name, sector));
    PrintTitle(title);
    printw("Funcionário cadastrado!\n\n");
    Print_Employee(employees->Tail->Data);
    refresh();
    Pause();
}

void Action_ShowEmployee()
{
    char title[] = "EXIBIR FUNCIONÁRIO";
    PrintTitle(title);

    int code;
    PromptForGreaterThanZeroInt("CÓDIGO DO FUNCIONÁRIO", &code);

    Employee* employee = Employee_FindInList(employees, code);

    if(employee == NULL)
    {
        ShowError("\nNão há funcionário cadastrado com o código informado!\n\n");
        Pause();
        return;
    }

    PrintTitle(title);
    Print_Employee(employee);
    refresh();
    Pause();
}

void Action_ListEmployees()
{
    char title[] = "FUNCIONÁRIOS CADASTRADOS";
    PrintTitle(title);

    if(employees->Length == 0)
    {
        ShowError("Não há funcionários cadastrados!\n\n");
        Pause();
        return;
    }

    List_ForEach(employees, Print_Employee);
    refresh();
    Pause();
}

//ITEM FUNCTIONS
bool Print_Item(void* data)
{
    Item* item = data;

    printw("CÓDIGO    : %d\n", item->Code);
    printw("NOME      : %s\n", item->Name);
    printw("QUANTIDADE: %d\n\n", item->Quantity);

    return true;
}

void Action_AddItem()
{
    char title[] = "CADASTRAR ITEM";
    PrintTitle(title);

    int code;
    char name[256];
    int quantity;

    PromptForGreaterThanZeroInt("CÓDIGO    ", &code);

    if(Item_FindInList(items, code) != NULL)
    {
        ShowError("Já existe um ítem cadastrado com esse código!\n\n");
        Pause();
        return;
    }

    PromptForNonEmptyString("NOME      ", name, 256);
    PromptForPositiveInt("QUANTIDADE", &quantity);

    Item_AddToList(items, Item_New(code, name, quantity));
    PrintTitle(title);
    ShowSucess("Item cadastrado!\n\n");
    Pause();
}

void Action_ShowItem()
{
    char title[] = "EXIBIR ITEM";
    PrintTitle(title);

    int code;
    PromptForGreaterThanZeroInt("CÓDIGO DO ITEM", &code);

    Item* item = Item_FindInList(items, code);

    if(item == NULL)
    {
        ShowError("Não existe item cadastrado com esse código!");
        Pause();
        return;
    }

    PrintTitle(title);
    Print_Item(item);
    refresh();
    Pause();
}

void Action_ListItems()
{
    char title[] = "LISTAR ITENS";
    PrintTitle(title);

    if(items->Length == 0)
    {
        ShowError("Não há itens cadastrados!\n\n");
        Pause();
        return;
    }

    List_ForEach(items, Print_Item);
    refresh();
    Pause();
    return;
}

void ShowMenu()
{
    Menu* menu = Menu_New("MENU PRINCIPAL");

    Option* employees_menu = Option_NewSubMenu("FUNCIONÁRIOS");
    Option_AddSubOption(employees_menu, Option_New("CADASTRAR", Action_AddEmployee));
    Option_AddSubOption(employees_menu, Option_New("EXIBIR", Action_ShowEmployee));
    Option_AddSubOption(employees_menu, Option_New("LISTAR", Action_ListEmployees));
    Menu_AddOption(menu,employees_menu);

    Option* items_menu = Option_NewSubMenu("ITENS");
    Option_AddSubOption(items_menu, Option_New("CADASTRAR", Action_AddItem));
    Option_AddSubOption(items_menu, Option_New("EXIBIR", Action_ShowItem));
    Option_AddSubOption(items_menu, Option_New("LISTAR", Action_ListItems));
    Menu_AddOption(menu, items_menu);

    Menu_Show(menu);
}

int main()
{    

    employees = Employee_NewEmployeeList();
    items = Item_NewItemList();
    transactions = Transaction_NewTransactionList();

    initscr();
    start_color();
    InitializeColors();    
    ShowMenu();
	endwin();   

	return 0;
}