#include "Transaction.h"

Transaction* Transaction_New(Employee* employee, Item* item, TransactionType type, int quantity)
{
    Transaction* transaction = malloc(sizeof(Transaction));

    transaction->Date = malloc(sizeof(time_t));
    time(transaction->Date);
    transaction->Employee = employee;
    transaction->Item = item;
    transaction->Type = type;
    transaction->Quantity = quantity;

    return transaction;
}

List* Transaction_NewTransactionList()
{
    return List_New(sizeof(Transaction), NULL, true);
}

void Transaction_AddToList(List* list, Transaction* transaction)
{
    List_Append(list, transaction);
}

bool CompareByItem(void* transaction_pointer, void* item_code_pointer)
{
    Transaction* transaction = transaction_pointer;
    int item_code = *(int*)item_code_pointer;

    return transaction->Item->Code == item_code;
}

List* Transaction_FindByItem(List* list, Item* item)
{
    return List_FindMany(list, CompareByItem, &item->Code);
}

bool CompareByEmployee(void* transaction_pointer, void* employee_code_pointer)
{
    Transaction* transaction = transaction_pointer;
    int employee_code = *(int*)employee_code_pointer;

    return transaction->Employee->Code == employee_code;
}

List* Transaction_FindByEmployee(List* list, Employee* employee)
{
    return List_FindMany(list, CompareByEmployee, &employee->Code);
}